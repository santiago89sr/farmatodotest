//
//  FarmatodoTestTests.swift
//  FarmatodoTestTests
//
//  Created by Santiago Rodriguez on 11/21/18.
//  Copyright © 2018 Santiago Rodriguez. All rights reserved.
//

import XCTest
@testable import FarmatodoTest

class FarmatodoTestTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testOperacionesMatematicas(){
        
        let instanceOfCustomObject: ObjectiveCBridge = ObjectiveCBridge()
        let result = instanceOfCustomObject.solveExpression("6+-(4)")?.intValue
        
        XCTAssertEqual(result, 2)
        
    }
    
    func testHallarMultiplos(){
        
        // 0 = characters
        // 3,5 = comics
        // 7 = creators
        // 11 = events
        // 13 = series
        // default, invalid = stories
        
        let vc:ViewController = ViewController()
        let result = vc.multiplos(resultado: "4")
        
        XCTAssertEqual(result, "stories")
        
    }


}
