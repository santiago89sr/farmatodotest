//
//  ObjectiveCBridge.h
//  FarmatodoTest
//
//  Created by Santiago Rodriguez on 11/21/18.
//  Copyright © 2018 Santiago Rodriguez. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ObjectiveCBridge: NSObject

-(NSNumber *)solveExpression:(NSString *)string;

@end
