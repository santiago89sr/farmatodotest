//
//  ViewController.swift
//  FarmatodoTest
//
//  Created by Santiago Rodriguez on 11/19/18.
//  Copyright © 2018 Santiago Rodriguez. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var expresionTF: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        expresionTF.text = ""
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if (textField.text?.isEmpty)! == false{
            algebraicOperation()
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let aSet = NSCharacterSet(charactersIn:"0123456789*/()+- ").inverted
        let compSepByCharInSet = string.components(separatedBy: aSet)
        let numberFiltered = compSepByCharInSet.joined(separator: "")
        return string == numberFiltered
    }
    
    func algebraicOperation(){
        
        
        let instanceOfCustomObject: ObjectiveCBridge = ObjectiveCBridge()
        let result = instanceOfCustomObject.solveExpression(expresionTF.text)
        
        if result != nil{
            self.pasarResultado(resultado: (result?.stringValue)!)
        }else{
            print("No es una expresion correcta!")
            pasarResultado(resultado: "fallo")
        }
        
    }
    
    public func multiplos(resultado:String) -> String{
        
        var nombreServicio:String!
        if let numero = Int(resultado){
            
            if numero == 0{
                nombreServicio = "characters"
            }else if (numero%3) == 0{
                nombreServicio = "comics"
            }else if (numero%5) == 0{
                nombreServicio = "comics"
            }else if (numero%7) == 0{
                nombreServicio = "creators"
            }else if (numero%11) == 0{
                nombreServicio = "events"
            }else if (numero%13) == 0{
                nombreServicio = "series"
            }else{
                nombreServicio = "stories"
            }
            
        }else{
            nombreServicio = "stories"
        }
        
        return nombreServicio
    }
    
    func pasarResultado(resultado:String){
        
        
        let nombreServicio = multiplos(resultado: resultado)
        
        
        let marvelCardsVC = self.storyboard?.instantiateViewController(withIdentifier: "MarvelCardsViewController") as! MarvelCardsViewController
        marvelCardsVC.resultado = nombreServicio
        self.navigationController?.pushViewController(marvelCardsVC, animated: true);
        
    }
    
    
    @IBAction func btnNext(_ sender: Any) {
        let marvelCardsVC = self.storyboard?.instantiateViewController(withIdentifier: "MarvelCardsViewController") as! MarvelCardsViewController
        
        self.navigationController?.pushViewController(marvelCardsVC, animated: true);
    }
    

}

