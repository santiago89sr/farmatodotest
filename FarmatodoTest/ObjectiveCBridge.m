//
//  ObjectiveCBridge.m
//  FarmatodoTest
//
//  Created by Santiago Rodriguez on 11/21/18.
//  Copyright © 2018 Santiago Rodriguez. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ObjectiveCBridge.h"


@implementation ObjectiveCBridge

-(NSNumber *)solveExpression:(NSString *)string{
    id value;
    @try {
        NSExpression *ex = [NSExpression expressionWithFormat:string];
        value = [ex expressionValueWithObject:nil context:nil];
    }
    @catch (NSException *e) { }
    return value;
}

@end
