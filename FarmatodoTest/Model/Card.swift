//
//  Character.swift
//  FarmatodoTest
//
//  Created by Santiago Rodriguez on 11/19/18.
//  Copyright © 2018 Santiago Rodriguez. All rights reserved.
//

import Foundation

struct Card: Decodable{
    
    let id: String
    let name: String
    let des: String
    let path: String
    let ext: String
    
}
