//
//  MarvelCardsCollectionViewCell.swift
//  FarmatodoTest
//
//  Created by Santiago Rodriguez on 11/19/18.
//  Copyright © 2018 Santiago Rodriguez. All rights reserved.
//

import UIKit
import SDWebImage

class MarvelCardsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imagen: UIImageView!
    
    var cardViewModel:CardsViewModel!{
        didSet{
            imagen.sd_setShowActivityIndicatorView(true)
            imagen.sd_setIndicatorStyle(.gray)
            imagen.sd_setImage(with: URL (string: cardViewModel.path + "." + cardViewModel.ext))
        }
    }
    
}
