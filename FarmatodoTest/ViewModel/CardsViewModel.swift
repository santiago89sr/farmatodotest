//
//  Character.swift
//  FarmatodoTest
//
//  Created by Santiago Rodriguez on 11/19/18.
//  Copyright © 2018 Santiago Rodriguez. All rights reserved.
//

import Foundation

struct CardsViewModel {
    
    let id: String
    let name: String
    let des: String
    let path: String
    let ext: String
    
    init(card:Card) {
        self.id = card.id
        self.name = card.name
        self.des = card.des
        self.path = card.path
        self.ext = card.ext
    }
    
    
}
