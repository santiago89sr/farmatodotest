//
//  MarvelCardsViewController.swift
//  FarmatodoTest
//
//  Created by Santiago Rodriguez on 11/19/18.
//  Copyright © 2018 Santiago Rodriguez. All rights reserved.
//

import UIKit

class MarvelCardsViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, DelegadoMarvel {
    
    
    var resultado:String!
    var cardsViewModels = [CardsViewModel]()
    var services = Services()
    var indicator:UIActivityIndicatorView!
    
    @IBOutlet weak var cardsCollectionView: UICollectionView!
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        services.delegado = self
        
        switch resultado {
        case "characters":
            services.serviceCharacters()
        case "comics":
            services.serviceComics()
        case "creators":
            services.serviceCreators()
        case "events":
            services.serviceEvents()
        case "series":
            services.serviceSeries()
        case "stories":
            services.serviceStories()
        default:
            services.serviceStories()
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        indicator = self.activityIndicatorStart()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return cardsViewModels.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MarvelCardsCollectionViewCell", for: indexPath) as! MarvelCardsCollectionViewCell
        
        let card = cardsViewModels[indexPath.row]
        cell.cardViewModel = card
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        collectionView.deselectItem(at: indexPath, animated: true)
        let card = cardsViewModels[indexPath.row]
        
        let detailCardVC = self.storyboard?.instantiateViewController(withIdentifier: "DetailCardViewController") as! DetailCardViewController
        
        detailCardVC.cardViewModel = card
        
        self.navigationController?.pushViewController(detailCardVC, animated: true);
        
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        //Ajuste tamaño celda.
        let height:CGFloat = collectionView.bounds.size.height
        let width:CGFloat = collectionView.bounds.size.width
        return CGSize(width:width, height: height)
    }
    
    func finalizaRespuesta(cards: [CardsViewModel]) {
        cardsViewModels = cards
        cardsCollectionView.reloadData()
        self.activityIndicatorStop(indicator: indicator)
    }
    
    func errorRespuesta(error: String) {
        self.activityIndicatorStop(indicator: indicator)
        print("Error: \(error)")
    }

    @IBAction func btnVolver(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
