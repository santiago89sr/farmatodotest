//
//  DetailCardViewController.swift
//  FarmatodoTest
//
//  Created by Santiago Rodriguez on 11/20/18.
//  Copyright © 2018 Santiago Rodriguez. All rights reserved.
//

import UIKit

class DetailCardViewController: UIViewController {
    
    var cardViewModel: CardsViewModel!

    @IBOutlet weak var infoLbl: UILabel!
    @IBOutlet weak var imagen: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        infoLbl.text = cardViewModel.name + "\n" + cardViewModel.des
        imagen.sd_setShowActivityIndicatorView(true)
        imagen.sd_setIndicatorStyle(.gray)
        imagen.sd_setImage(with: URL (string: cardViewModel.path + "." + cardViewModel.ext))
        
    }
    
    @IBAction func btnVolver(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    

}
