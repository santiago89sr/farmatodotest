//
//  UIViewController+ActivityIndicator.swift
//  FarmatodoTest
//
//  Created by Santiago Rodriguez on 11/21/18.
//  Copyright © 2018 Santiago Rodriguez. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController{
    
    func activityIndicatorStart() -> UIActivityIndicatorView {
        
        let indicator = UIActivityIndicatorView(style: .gray)
        self.view.addSubview(indicator)
        indicator.center = self.view.center
        indicator.startAnimating()
        
        return indicator
        
    }
    
    func activityIndicatorStop(indicator:UIActivityIndicatorView){
        
        indicator.hidesWhenStopped = true
        indicator.stopAnimating()
        
    }
    
}
