//
//  Services.swift
//  FarmatodoTest
//
//  Created by Santiago Rodriguez on 11/19/18.
//  Copyright © 2018 Santiago Rodriguez. All rights reserved.
//

import Foundation
import Alamofire
import Alamofire_SwiftyJSON

protocol DelegadoMarvel {
    func finalizaRespuesta(cards:[CardsViewModel])
    func errorRespuesta(error:String)
}

class Services {
    
    var delegado:DelegadoMarvel?
    
    // MARK: Characters
    func serviceCharacters(){
        
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = TimeInterval(10)
        
        let request = "http://gateway.marvel.com/v1/public/characters?apikey=1e229818860f7081c532e634d8f6ae65&ts=1504796200286&hash=e02be810b65957f4b60b71ca89b8835f&offset=0&limit=20"
        
        manager.request(request, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseSwiftyJSON { dataResponse in
            
            if dataResponse.result.isFailure{
                self.delegado?.errorRespuesta(error: "Fallo el servicio")
                return
            }else{
                
                if dataResponse.error == nil {
                    
                    if let marvel_json = dataResponse.value{
                        
                        if marvel_json["code"].intValue == 200{//Exitoso
                            let data_json = marvel_json["data"]
                            let results_json = data_json["results"].array
                            var cards = [CardsViewModel]()
                            for result in results_json!{
                                let id = result["id"].stringValue
                                let name = result["name"].stringValue
                                let des = result["description"].stringValue
                                let thumbnail = result["thumbnail"]
                                
                                let path = thumbnail["path"].stringValue
                                let ext = thumbnail["extension"].stringValue
                                
                                let card = Card.init(id: id, name: name, des: des, path: path, ext: ext)
                                let cardVM = CardsViewModel.init(card: card)
                                cards.append(cardVM)
                            }
                            self.delegado?.finalizaRespuesta(cards: cards)
                            
                        }
                        
                    }
                    
                }else{
                    self.delegado?.errorRespuesta(error: dataResponse.error as! String)
                }
                
            }
            
        }
        
    }
    
    // MARK: Comics
    func serviceComics(){
        
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = TimeInterval(10)
        
        let request = "http://gateway.marvel.com/v1/public/comics?apikey=1e229818860f7081c532e634d8f6ae65&ts=1504796200286&hash=e02be810b65957f4b60b71ca89b8835f&offset=0&limit=20"
        
        manager.request(request, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseSwiftyJSON { dataResponse in
            
            if dataResponse.result.isFailure{
                self.delegado?.errorRespuesta(error: "Fallo el servicio")
                return
            }else{
                
                if dataResponse.error == nil {
                    
                    if let marvel_json = dataResponse.value{
                        
                        if marvel_json["code"].intValue == 200{//Exitoso
                            let data_json = marvel_json["data"]
                            let results_json = data_json["results"].array
                            var cards = [CardsViewModel]()
                            for result in results_json!{
                                let id = result["id"].stringValue
                                let name = result["title"].stringValue
                                let des = result["description"].stringValue
                                let thumbnail = result["thumbnail"]
                                
                                let path = thumbnail["path"].stringValue
                                let ext = thumbnail["extension"].stringValue
                                
                                let card = Card.init(id: id, name: name, des: des, path: path, ext: ext)
                                let cardVM = CardsViewModel.init(card: card)
                                cards.append(cardVM)
                            }
                            self.delegado?.finalizaRespuesta(cards: cards)
                            
                        }
                        
                    }
                    
                }else{
                    self.delegado?.errorRespuesta(error: dataResponse.error as! String)
                }
                
            }
            
        }
        
    }
    
    // MARK: - Creators
    func serviceCreators(){
        
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = TimeInterval(10)
        
        let request = "http://gateway.marvel.com/v1/public/creators?apikey=1e229818860f7081c532e634d8f6ae65&ts=1504796200286&hash=e02be810b65957f4b60b71ca89b8835f&offset=0&limit=20"
        
        manager.request(request, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseSwiftyJSON { dataResponse in
            
            if dataResponse.result.isFailure{
                self.delegado?.errorRespuesta(error: "Fallo el servicio")
                return
            }else{
                
                if dataResponse.error == nil {
                    
                    if let marvel_json = dataResponse.value{
                        
                        if marvel_json["code"].intValue == 200{//Exitoso
                            let data_json = marvel_json["data"]
                            let results_json = data_json["results"].array
                            var cards = [CardsViewModel]()
                            for result in results_json!{
                                let id = result["id"].stringValue
                                let name = result["fullName"].stringValue
                                let des = result["suffix"].stringValue
                                let thumbnail = result["thumbnail"]
                                
                                let path = thumbnail["path"].stringValue
                                let ext = thumbnail["extension"].stringValue
                                
                                let card = Card.init(id: id, name: name, des: des, path: path, ext: ext)
                                let cardVM = CardsViewModel.init(card: card)
                                cards.append(cardVM)
                            }
                            self.delegado?.finalizaRespuesta(cards: cards)
                            
                        }
                        
                    }
                    
                }else{
                    self.delegado?.errorRespuesta(error: dataResponse.error as! String)
                }
                
            }
            
        }
        
    }
    
    // MARK: - Events
    func serviceEvents(){
        
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = TimeInterval(10)
        
        let request = "http://gateway.marvel.com/v1/public/events?apikey=1e229818860f7081c532e634d8f6ae65&ts=1504796200286&hash=e02be810b65957f4b60b71ca89b8835f&offset=0&limit=20"
        
        manager.request(request, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseSwiftyJSON { dataResponse in
            
            if dataResponse.result.isFailure{
                self.delegado?.errorRespuesta(error: "Fallo el servicio")
                return
            }else{
                
                if dataResponse.error == nil {
                    
                    if let marvel_json = dataResponse.value{
                        
                        if marvel_json["code"].intValue == 200{//Exitoso
                            let data_json = marvel_json["data"]
                            let results_json = data_json["results"].array
                            var cards = [CardsViewModel]()
                            for result in results_json!{
                                let id = result["id"].stringValue
                                let name = result["title"].stringValue
                                let des = result["description"].stringValue
                                let thumbnail = result["thumbnail"]
                                
                                let path = thumbnail["path"].stringValue
                                let ext = thumbnail["extension"].stringValue
                                
                                let card = Card.init(id: id, name: name, des: des, path: path, ext: ext)
                                let cardVM = CardsViewModel.init(card: card)
                                cards.append(cardVM)
                            }
                            self.delegado?.finalizaRespuesta(cards: cards)
                            
                        }
                        
                    }
                    
                }else{
                    self.delegado?.errorRespuesta(error: dataResponse.error as! String)
                }
                
            }
            
        }
        
    }
    
    // MARK: - Series
    func serviceSeries(){
        
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = TimeInterval(10)
        
        let request = "http://gateway.marvel.com/v1/public/series?apikey=1e229818860f7081c532e634d8f6ae65&ts=1504796200286&hash=e02be810b65957f4b60b71ca89b8835f&offset=0&limit=20"
        
        manager.request(request, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseSwiftyJSON { dataResponse in
            
            if dataResponse.result.isFailure{
                self.delegado?.errorRespuesta(error: "Fallo el servicio")
                return
            }else{
                
                if dataResponse.error == nil {
                    
                    if let marvel_json = dataResponse.value{
                        
                        if marvel_json["code"].intValue == 200{//Exitoso
                            let data_json = marvel_json["data"]
                            let results_json = data_json["results"].array
                            var cards = [CardsViewModel]()
                            for result in results_json!{
                                let id = result["id"].stringValue
                                let name = result["title"].stringValue
                                let des = result["description"].stringValue
                                let thumbnail = result["thumbnail"]
                                
                                let path = thumbnail["path"].stringValue
                                let ext = thumbnail["extension"].stringValue
                                
                                let card = Card.init(id: id, name: name, des: des, path: path, ext: ext)
                                let cardVM = CardsViewModel.init(card: card)
                                cards.append(cardVM)
                            }
                            self.delegado?.finalizaRespuesta(cards: cards)
                            
                        }
                        
                    }
                    
                }else{
                    self.delegado?.errorRespuesta(error: dataResponse.error as! String)
                }
                
            }
            
        }
        
    }
    
    // MARK: - Stories
    func serviceStories(){
        
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = TimeInterval(10)
        
        let request = "http://gateway.marvel.com/v1/public/stories?apikey=1e229818860f7081c532e634d8f6ae65&ts=1504796200286&hash=e02be810b65957f4b60b71ca89b8835f&offset=0&limit=20"
        
        manager.request(request, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseSwiftyJSON { dataResponse in
            
            if dataResponse.result.isFailure{
                self.delegado?.errorRespuesta(error: "Fallo el servicio")
                return
            }else{
                
                if dataResponse.error == nil {
                    
                    if let marvel_json = dataResponse.value{
                        
                        if marvel_json["code"].intValue == 200{//Exitoso
                            let data_json = marvel_json["data"]
                            let results_json = data_json["results"].array
                            var cards = [CardsViewModel]()
                            for result in results_json!{
                                let id = result["id"].stringValue
                                let name = result["title"].stringValue
                                let des = result["description"].stringValue
                                let thumbnail = result["thumbnail"]
                                
                                let path = thumbnail["path"].stringValue
                                let ext = thumbnail["extension"].stringValue
                                
                                let card = Card.init(id: id, name: name, des: des, path: path, ext: ext)
                                let cardVM = CardsViewModel.init(card: card)
                                cards.append(cardVM)
                            }
                            self.delegado?.finalizaRespuesta(cards: cards)
                            
                        }
                        
                    }
                    
                }else{
                    self.delegado?.errorRespuesta(error: dataResponse.error as! String)
                }
                
            }
            
        }
        
    }
    
}
